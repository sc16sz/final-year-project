import numpy as np
import pandas as pd
from scipy import stats, integrate
import matplotlib.pyplot as plt
import warnings
import seaborn as sns
import sys

def draw_probability_graph(a):

    print(a)

    a[0] = a[0][1:]
    a[-1] = a[-1][:-1]

    print(a)

    for i in range(0, len(a)):
        if i != len(a)-1:
            a[i] = a[i][:-1]
            a[i] = float(a[i])
        else:
            a[i] = float(a[i])


    print(a)
    sns.kdeplot(a)

    plt.title('Probability Distribution Graph')
    plt.xlabel('Time Consumed')
    plt.ylabel('Probability')
    plt.xlim((0,max(a)+1))

    plt.show()

#a=[1,1,1,1,2,2,2,3,3,3,4,5,6,7,8,5,6,5,4,8,9,5,6,5,4,1,8,5,5,8,5,8,7]
draw_probability_graph(sys.argv[1:])