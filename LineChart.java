package org.processmining.models.graphbased.directed.fuzzymodel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


public class LineChart extends JFrame{
	
	  private static final long serialVersionUID = 1L;


	   public LineChart( String title, String sourceName, String targetName, List<Double> duration, ArrayList<Double> results ) {
	      super(title);
	      JFreeChart lineChart = ChartFactory.createXYLineChart(
	         title,
	         "Time Durations","Probability",
	         createDataset(duration),
	         PlotOrientation.VERTICAL,
	         true,true,false);
	         
		  lineChart.getXYPlot().setRenderer(new XYSplineRenderer());
	      ChartPanel chartPanel = new ChartPanel( lineChart );
	      chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
	      setContentPane( chartPanel );
	   }

	   private XYDataset createDataset(List<Double> duration) {
		   
	        // Probability density 
	        double Pd;  
	        // Array list size
	        int framenum = duration.size();
	        // Number of frames in each section 
	        int count = 0; 
	        // Number of sections
	        int intervalCount = 0;
	        if (framenum <= 6) {
	        	intervalCount = framenum;
	        }else {
	        	if (framenum % 3 == 0) {
	            	intervalCount = framenum / 3;
	            }else if (framenum % 3 == 1) {
	            	intervalCount = (framenum + 2) / 3;
	            }else {
	            	intervalCount = (framenum + 1) / 3;
	            }
	        }   
	        
	        // Array that stores coordinates 
	        double[][] frameArray = new double[intervalCount][2];
	        
	        List<Double> upIntervalList = new ArrayList<>();
	        List<Double> downIntervalList = new ArrayList<>();
	        List<Double> middleValueList = new ArrayList<>();
	        double upInterval, downInterval, middleValue; 

	        Collections.sort(duration);
	  
	        double minFrame = duration.get(0);  
	        double maxFrame = duration.get(framenum - 1);  
	        // Width of each section
	        BigDecimal bd = new BigDecimal((maxFrame - minFrame) / intervalCount);
	        double interval = bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();  
	        
	          
	  
	        for (int k = 0; k < intervalCount; k++) {  
	  
	            bd = new BigDecimal(minFrame + (k + 1) * interval - 1);
	            upInterval = bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
	            upIntervalList.add(upInterval);
	            bd = new BigDecimal(minFrame + k * interval);
	            downInterval = bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
	            downIntervalList.add(downInterval);
	            
	            //System.out.println("Min=" + downInterval + " " + "Max=" + upInterval);
	            
	            middleValue = downInterval + interval / 2; // Middle point of each section (horizontal coordinate) 
	            middleValueList.add(middleValue);
	  
	            for (int i = 0; i < framenum; i++) {  
	                if (duration.get(i) < upInterval && duration.get(i) >= downInterval) {  
	                    count++;  
	                }  
	            }  
	            Pd = (double) count / framenum; 
	            frameArray[k][0] = middleValue;  
	            frameArray[k][1] = Pd;  
	            count = 0;  
	        }    
		   
		  XYSeriesCollection dataset = new XYSeriesCollection();
		  XYSeries series1 = new XYSeries("Time Consumption");
		  
		  for (int i = 0; i < intervalCount; i++) {
			  if ( frameArray[i][1] >= 0.05 ) { // Drop those values with low probability
				  series1.add(frameArray[i][0], frameArray[i][1]);
			  }			  
		  }
	      dataset.addSeries(series1);
	      return dataset;
	   }
	   
//	   public static void main( String[ ] args ) {
//	      LineChart chart = new LineChart(
//	         "School Vs Years" ,
//	         "Numer of Schools vs years");
//
//	      chart.pack( );
//	      RefineryUtilities.centerFrameOnScreen( chart );
//	      chart.setVisible( true );
//	   }
}
