package org.processmining.models.graphbased.directed.fuzzymodel;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;

public class DensityFunctionGraph extends Thread{
	
	private List<Double> duration;
	
	
	public DensityFunctionGraph(List<Double> duration) {
		// TODO Auto-generated constructor stub
		this.duration = duration;
    }
	
	public void run() {
		File directory = new File(".");
		try {
		    System.out.println(directory.getCanonicalPath());
		    BufferedReader br = null;
		    try {
		    	System.out.println(duration);
		    	Process p = Runtime.getRuntime().exec("cmd.exe /c python " + directory.getCanonicalPath() + "\\DensityGraph.py " + duration);
		    	br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    	String line = null;
		    	while ((line = br.readLine()) != null) {
		    		System.out.println(line);
		    	}
		    } catch (Exception e) {
		    	e.printStackTrace();
		    } finally {
 
		    	if (br != null) {
		    		try {
                	br.close();
		    		} catch (Exception e) {
		    			e.printStackTrace();
		    		}
		    	}
        }
		    
		}catch(Exception e){} 
		directory.delete();
	}
		
	
//	public static void main(String[] args) throws IOException, InterruptedException {
//        
//		
//		ArrayList<Double> duration = new ArrayList<Double>();
//		duration.add(1.0);
//		duration.add(2.0);
//		duration.add(3.0);
//		duration.add(3.0);
//		duration.add(4.0);
//		duration.add(3.0);
//		duration.add(2.0);
//		duration.add(2.0);
//		duration.add(2.0);
//		duration.add(2.0);
//		duration.add(2.0);
//		
//		
//		File directory = new File(".");
//		try {
//		    System.out.println(directory.getCanonicalPath());
//		    BufferedReader br = null;
//		    try {
//		    	System.out.println(duration);
//		    	Process p = Runtime.getRuntime().exec("cmd.exe /c python " + directory.getCanonicalPath() + "\\DensityGraph.py " + duration);
//		    	br = new BufferedReader(new InputStreamReader(p.getInputStream()));
//		    	String line = null;
//		    	while ((line = br.readLine()) != null) {
//		    		System.out.println(line);
//		    	}
//		    } catch (Exception e) {
//		    	e.printStackTrace();
//		    } finally {
// 
//		    	if (br != null) {
//		    		try {
//                	br.close();
//		    		} catch (Exception e) {
//		    			e.printStackTrace();
//		    		}
//		    	}
//        }
//		    
//		}catch(Exception e){} 
//		directory.delete();
//        
//        
//    }

	

}
