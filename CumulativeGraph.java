package org.processmining.models.graphbased.directed.fuzzymodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class CumulativeGraph extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CumulativeGraph(String title, String sourceName, String targetName, List<Double> duration, ArrayList<Double> results) {
		// TODO Auto-generated constructor stub
		super(title);
		
		 // Create dataset
	    XYDataset dataset = createScatterPlotDataset(duration);

	    // Create chart
	    JFreeChart chart = ChartFactory.createScatterPlot(
	        "Scatter Plot From " + sourceName + " To " + targetName, 
	        "Time Consumption", "Activity No.", dataset);
	   
	    // Create Panel
	    JPanel panel = new ChartPanel(chart);
	    panel.setPreferredSize(new java.awt.Dimension(500, 270));
	    setContentPane(panel);
	    

	    
	}

	  private XYDataset createScatterPlotDataset(List<Double> duration) {
	    XYSeriesCollection dataset = new XYSeriesCollection();

	    // series
	    XYSeries series1 = new XYSeries("Time Consumption");
	    for (int i = 0; i < duration.size(); i++) {
	    	series1.add(duration.get(i).doubleValue(), i + 1);
	    }
	    dataset.addSeries(series1);

	    return dataset;
	  }  

//	  public static void main(String[] args) {
//	    SwingUtilities.invokeLater(() -> {
//	      AccumulationGraph example = new AccumulationGraph("Scatter Chart");
//	      example.setSize(800, 400);
//	      example.setLocationRelativeTo(null);
//	      example.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//	      example.setVisible(true);
//	    });
//	  }

}
