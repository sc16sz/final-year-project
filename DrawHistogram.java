package org.processmining.models.graphbased.directed.fuzzymodel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.jfree.data.xy.IntervalXYDataset;



public class DrawHistogram extends JFrame{
	private static final long serialVersionUID = 1L;
	
	/**
     * Creates a new demo.
     * 
     * @param title  the frame title.
     */
    public DrawHistogram(String title, String sourceName, String targetName, List<Double> duration, ArrayList<Double> results) {
		// TODO Auto-generated constructor stub
    	super(title);       	
        JPanel chartPanel = createDemoPanel(title, duration, results);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
	}
    
    
    
    
    /**
     * Creates a sample {@link HistogramDataset}.
     * 
     * @return the dataset.
     */
    private static IntervalXYDataset initDataset(String title, List<Double> duration) {
        // Array list size
        int framenum = duration.size();
        // Number of sections
        int intervalCount = 0;
        if (framenum <= 6) {
        	intervalCount = framenum;
        }else {
        	if (framenum % 3 == 0) {
            	intervalCount = framenum / 3;
            }else if (framenum % 3 == 1) {
            	intervalCount = (framenum + 2) / 3;
            }else {
            	intervalCount = (framenum + 1) / 3;
            }
        }          
        
        double[] dOfDuration = new double[duration.size()];
        for (int i = 0; i < framenum; i++) {
        	dOfDuration[i] =  duration.get(i).doubleValue()/framenum ;
        	//System.out.println(dOfDuration[i]);
        }
        HistogramDataset dataSet = new HistogramDataset();
        dataSet.setType(HistogramType.FREQUENCY);
        dataSet.addSeries(title, dOfDuration, intervalCount);

        //dataset.addObservations(dOfDuration);
        return dataSet;     
    }
    
    /**
     * Creates a chart.
     * 
     * @param dataset  a dataset.
     * 
     * @return The chart.
     */
    private static JFreeChart createChart(String title, IntervalXYDataset dataset) {
        JFreeChart chart = ChartFactory.createHistogram(
        		title, "TIme Duration", "Probability", dataset, 
                PlotOrientation.VERTICAL, true, true, false);
        return chart;
    }
        
    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     * 
     * @return A panel.
     */
    public static JPanel createDemoPanel(String title, List<Double> duration, ArrayList<Double> results) {
        JFreeChart chart = createChart(title, initDataset(title, duration));
        String label;
        if (results.size() != 0) {
			label = " Mean time: "   + results.get(0) + "Min\n"
						+ " Median time: " + results.get(1) + "Min\n"
						+ " LowerQuartile time: " + results.get(2) + "Min\n"
						+ " UpperQuartile time: " + results.get(3) + "Min\n"; 
			chart.addSubtitle(new TextTitle(label));
        }       
        return new ChartPanel(chart);
    }
    
    /**
     * The starting point for the demo.
     * 
     * @param args  ignored.
     */
//    public static void main(String[] args) {
//        DrawHistogram demo = new DrawHistogram(
//                "Simple Histogram Demo");
//        demo.pack();
//        RefineryUtilities.centerFrameOnScreen(demo);
//        demo.setVisible(true);
//    }

}
