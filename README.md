# Final Year Project

Only includes created files, modified files and dataset 1 in XES format.


Step 1:
Checking out the Fuzzy miner package from 'trunk' under https://svn.win.tue.nl/repos/prom/Packages/Fuzzy through SVN in Eclipse

Step 2:
According to the relative paths shown below, move the files into the Fuzzy package and replace existed file if necessary.
*  **DensityGraph.py** – Drawing density curves in Python.
Path: ~\Fuzzy\DensityGraph.py
*  **CalDateValues.java** – Extracting data from event log, rearranging the data into sorted lists and calculate mean, median and quartiles.
Path:~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ CalDateValues.java
*  **CumulativeGraph.java** – Drawing cumulative scatter plot
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ CumulativeGraph.java
*  **DensityFunctionGraph.java** – Calling the python script to draw density curve
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ DensityFunctionGraph.java
*  **DrawHistogram.java** – Drawing histograms
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ DrawHistogram.java
*  **Global.java** – keeping the rearranged sorted lists and providing them to methods that need them
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ Global.java
* **LineChart.java** – Drawing simulated density curves
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ LineChart.java 
*  **MutableFuzzyGraph.java** – Inserting the graphs while the model is generating
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\ MutableFuzzyGraph.java
* **FMEdgeImpl.java** – Adding labels to model’s edges to correspond graphs with events
Path: ~\Fuzzy\src\org\processmining\models\graphbased\directed\fuzzymodel\impl\ FMEdgeImpl.java  

Step 3：
Reconfigure the project and run the program --> plug-in can be used
