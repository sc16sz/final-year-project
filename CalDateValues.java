package org.processmining.models.graphbased.directed.fuzzymodel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.jfree.ui.RefineryUtilities;

public class CalDateValues {
	
	//---------------------------------
	// Data field 
	private XLog log;
	private String sourceName;
	private String targetName;
	public ArrayList<Double> results = new ArrayList<>();
	
	
	//----------------------------------
	// Constructors
	public CalDateValues(XLog log, String source, String target) {
		this.log = log;
		this.sourceName = source;
		this.targetName = target;
	}
	

	//----------------------------------
	// Local methods
	/** 
     * Calculate the Mean, median, lower and upper quartiles of given list 
     * 
     * @return  results		calculated result
     */  
	public List<Double> returnResults(){

		List<Double> resultDuration = this.getDuration();
		// Based on the stored duration to calculate
		if (!resultDuration.isEmpty()) {
					
			results.add(this.getMean(resultDuration));
			results.add(this.getMedian(resultDuration));
			results.add(this.getLowerQuartile(resultDuration));
			results.add(this.getUpperQuartile(resultDuration));
			
			if (!Global.EDGE_NAME_LIST.contains(sourceName + "+" + targetName)) {
				Global.EDGE_NAME_LIST.add(sourceName + "+" + targetName);
				//System.out.println(sourceName + "+" + targetName);
				this.setGrapg();
			}
			
		}
		return results;
	}

	public List<Double> getDuration(){	
		// Define some intermediate variables
		XTrace xTrace;
		XEvent xEvent;
		
		Date startTime = new Date();
		double durations;
		int flag;
		List<Double> resultDuration = new ArrayList<>(); /* will store all duration time between 2 provided events*/
		
		//iterate the log file to calculate the duration between source and target event
		for (int i = 0; i < log.size(); i++) {
			xTrace = log.get(i); // Get one of the ID patients' trace
			flag = 0;
			for (int j = 0; j < xTrace.size(); j++) {
				xEvent = xTrace.get(j); // Get one of the events from the trace
				// Generate a map containing all the events' attribute 
				Map<String,XAttribute> xAttrMap = xEvent.getAttributes(); 
				// Judge if it is the source or the target
				if (xAttrMap.get("concept:name").toString().equals(sourceName)) {
					XAttributeTimestamp xAttriTime = (XAttributeTimestamp) xAttrMap.get("time:timestamp");				
					startTime = xAttriTime.getValue();
					flag = 1;
				}else if (xAttrMap.get("concept:name").toString().equals(targetName) && flag == 1){
					XAttributeTimestamp xAttriTime = (XAttributeTimestamp) xAttrMap.get("time:timestamp");				
					durations = (int) ((xAttriTime.getValue().getTime() - startTime.getTime()) / (1000 * 60));
					resultDuration.add(durations);
				}
			}		
		}		
		
		return resultDuration;
	}
	
	public void setGrapg() {
		
		List<Double> duration = this.getDuration();
		
		Collections.sort(duration);;
		
		DrawHistogram drawHistogram = new DrawHistogram("Graph" + (Global.EDGE_NAME_LIST.indexOf(sourceName + "+" + targetName) + 1)
		 + " From " + sourceName + " To " + targetName, sourceName, targetName, duration, results);
		drawHistogram.pack();
		RefineryUtilities.centerFrameOnScreen(drawHistogram);
		drawHistogram.setVisible(true);
		
		CumulativeGraph aGraph = new CumulativeGraph("Graph" + (Global.EDGE_NAME_LIST.indexOf(sourceName + "+" + targetName) + 1)
		 + " From " + sourceName + " To " + targetName, sourceName, targetName, duration, results);
		aGraph.pack();
		RefineryUtilities.centerFrameOnScreen(aGraph);
		aGraph.setVisible(true);
		
//		BarAndLineChart blChart = new BarAndLineChart("Graph" + (Global.EDGE_NAME_LIST.indexOf(sourceName + "+" + targetName) + 1)
//		 + " From " + sourceName + " To " + targetName, sourceName, targetName, duration, results);
//		blChart.pack();
//		RefineryUtilities.centerFrameOnScreen(blChart);
//		blChart.setVisible(true);
		
		LineChart lChart = new LineChart("Graph" + (Global.EDGE_NAME_LIST.indexOf(sourceName + "+" + targetName) + 1)
		 + " From " + sourceName + " To " + targetName, sourceName, targetName, duration, results);
		lChart.pack();
		RefineryUtilities.centerFrameOnScreen(lChart);
		lChart.setVisible(true);
		
		DensityFunctionGraph dfGraph = new DensityFunctionGraph(duration);
		dfGraph.start();
		
		
		
	}

    
	// This is calculation process of finding Means, Median, lower-quartile and upper-quartile
	
    /** 
     * Calculate  the mean of the given array 
     * @param arr	Array where to find the mean
     * 
     * @return mean of the given array
     */  
	private static double getMean(List<Double> arr) {
		BigDecimal bd;
		double total = 0;
		
		for(Iterator<Double> it = arr.iterator();it.hasNext();){
            total = total + it.next();
        }		
		bd = new BigDecimal(total/arr.size());			
		return bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/** 
     * Calculate the median of the given array
     * @param arr	Array where to find the median
     * 
     * @return median of the array
     */  
	private static double getMedian(List<Double> arr) {
		Collections.sort(arr);			
		BigDecimal bd;
		int size = arr.size();
		
	    if(size % 2 == 1){
	    	bd = new BigDecimal(arr.get((size-1)/2));
	    }else {
	    	bd = new BigDecimal((arr.get(size/2-1) + arr.get(size/2))/2);
	    }
		return bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/** 
     * Calculate the upper-quartile of the given array 
     * @param arr	Array where to find the lower quartile
     * 
     * @return lower quartile of the array
     */  
	private static double getLowerQuartile(List<Double> arr) {
		Collections.sort(arr);			
		BigDecimal bd;
		int size = arr.size();
		
		if (size == 1){
			bd = new BigDecimal(arr.get(0));
        }else if (size == 2){
        	bd = new BigDecimal(arr.get(0));
        }else if (size == 3){
        	bd = new BigDecimal(arr.get(0));
        }else {
        	if (size % 2 == 1){
                int i = (size - 1) / 2;
                if (i % 2 == 1){
                	bd = new BigDecimal(arr.get((i-1)/2));
                }else {
                	bd = new BigDecimal((arr.get(i-i/2)+arr.get(i-i/2-1))/2);
                }
            }else {
                int i = (size - 1) / 2;
                if (i % 2 == 1){
                	bd = new BigDecimal(arr.get(size-2)/4);
                }else {
                	bd = new BigDecimal((arr.get(size/4-1)+arr.get(size/4)/2));
                }
            }
        }           
		return bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/** 
     * Calculate the upper-quartile of the given array
     * @param arr	Array where to find the upper quartile
     * 
     * @return upper quartile of the array	
     */  
	private static double getUpperQuartile(List<Double> arr) {
		Collections.sort(arr);			
		BigDecimal bd;
		int size = arr.size();
		
		if (size == 1){
			bd = new BigDecimal(arr.get(0));
        }else if (size == 2){
        	bd = new BigDecimal(arr.get(1));
        }else if (size == 3){
        	bd = new BigDecimal(arr.get(2));
        }else {
            if (size % 2 == 1){
                int i = (size - 1) / 2;
                if (i % 2 == 1){
                	bd = new BigDecimal(arr.get((3*size-1)/4));
                }else {
                	bd = new BigDecimal((arr.get(i+i/2)+arr.get(i+i/2+1))/2);
                }
            }else { 
                int i = (size - 1) / 2;
                if (i % 2 == 1){ 
                	bd = new BigDecimal(arr.get((size*3-2)/4));
                }else {
                	bd = new BigDecimal((arr.get(3*size/4-1)+arr.get(3*size/4))/2);
                }
            }
        }
		return bd.setScale(1,BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
}
