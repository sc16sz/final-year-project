package org.processmining.models.graphbased.directed.fuzzymodel.impl;

import java.util.List;

import org.processmining.models.graphbased.AttributeMap;
import org.processmining.models.graphbased.AttributeMap.ArrowType;
import org.processmining.models.graphbased.directed.fuzzymodel.FMEdge;
import org.processmining.models.graphbased.directed.fuzzymodel.FMNode;
import org.processmining.models.graphbased.directed.fuzzymodel.Global;
import org.processmining.models.graphbased.directed.fuzzymodel.MutableFuzzyGraph;


public class FMEdgeImpl extends FMEdge<FMNode, FMNode> {
	public FMEdgeImpl(FMNode source, FMNode target, double significance, double correlation, List<Double> calResults) {

		super(source, target, significance, correlation);
		
		// Test if it is used
		System.out.print("This impl class is used for generating edges\n");	
		getAttributeMap().put(AttributeMap.EDGEEND, ArrowType.ARROWTYPE_TECHNICAL);
		getAttributeMap().put(AttributeMap.EDGEENDFILLED, true);		
		getAttributeMap().put(AttributeMap.LABELALONGEDGE, true);
		
		getAttributeMap().put(AttributeMap.SHOWLABEL, true);
		String label;
		if (calResults.size() != 0) {
			label = "cooresponding graph see 'Graph " 
						 + (Global.EDGE_NAME_LIST.indexOf(source.getElementName() + "+" + target.getElementName()) + 1)
						 + "'\n";
		}else {
			label = "significance: " + MutableFuzzyGraph.format(significance) 
						 + " correlation: "+ MutableFuzzyGraph.format(correlation) + "\n";
		}
		getAttributeMap().put(AttributeMap.LABEL, label);	
	}

	public FMEdgeImpl(FMNode source, FMNode target, double significance, double correlation) {

		super(source, target, significance, correlation);
		
		// Test if it is used
		//System.out.print("This impl class is used for generating edges\n");	
		getAttributeMap().put(AttributeMap.EDGEEND, ArrowType.ARROWTYPE_TECHNICAL);
		getAttributeMap().put(AttributeMap.EDGEENDFILLED, true);		
		getAttributeMap().put(AttributeMap.LABELALONGEDGE, true);
		
		getAttributeMap().put(AttributeMap.SHOWLABEL, true);
		String label = "significance: " + MutableFuzzyGraph.format(significance) 
						 + "\ncorrelation: "+ MutableFuzzyGraph.format(correlation) + "\n";
		getAttributeMap().put(AttributeMap.LABEL, label);	
	}

	
	public String toString() {
//		String sourceLabel = source.getElementName() + "(" + source.getEventType() + ")";
//		String targetLabel = target.getElementName() + "(" + target.getEventType() + ")";
//		String edgeLabel = sourceLabel + "-->" + targetLabel;
		
		/* 
		 * HV: When any text here is returned, double-clicking an edge will throw a null-pointer exception.
		 * This exception is caused by the lack of font metrics to decide whether or not this text could
		 * have been selected by the double-click. By returning an empty string here, we prevent the 
		 * exception.
		 * 
		 *  Of course, this is not the proper way to do this, but at the moment I see no other way.
		 *  The only method that can set the metrics, is getLabelSize, but for that we need an EdgeRenderer an 
		 *  an EdgeView.
		 */
		return "";
	}
}
